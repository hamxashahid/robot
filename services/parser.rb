# frozen_string_literal: true

class Robot::Parser

  def initialize(robot:, surface:)
    self.robot = robot
    self.surface = surface
  end

  def call(command:)
    parse(command)
  end

  private

  attr_accessor :robot, :surface

  def parse(command)
    if command =~ /^PLACE\s+\d+\s*,\s*\d+\s*,\s*(NORTH|SOUTH|EAST|WEST)$/
      command, x, y, direction = command.gsub(',', ' ').split
      command_type = 'place'
      params = {x: x, y: y, direction: direction}
    elsif command =~ /^MOVE$/
      command_type = 'move'
    elsif command =~ /^LEFT$/
      command_type = 'left'
    elsif command =~ /^RIGHT$/
      command_type = 'right'
    elsif command =~ /^REPORT$/
      command_type = 'report'
    end
    Robot::Command.new(
        robot: robot,
        surface: surface,
        command_type: command_type,
        params: params,
    ) unless command_type.nil?
  end
end
