# frozen_string_literal: true

class Robot::Command

  def initialize(robot:, surface:, command_type:, params:)
    self.robot = robot
    self.surface = surface
    self.command_type = command_type
    self.params = params
  end

  def call
    self.send(command_type)
  end

  private

  attr_accessor :robot, :surface, :command_type, :params

  def place
    position = Position.new(
        x: params[:x].to_i,
        y: params[:y].to_i,
        direction: params[:direction],
    )
    robot.position = position if surface.position_valid?(position: position)
  end

  def move
    return unless robot.placed?
    new_position = robot.position.move(direction: robot.position&.direction&.downcase)
    robot.position = new_position if surface.position_valid?(position: new_position)
  end

  def left
    return unless robot.placed?
    robot.position.direction = robot.position.turn_left
  end

  def right
    return unless robot.placed?
    robot.position.direction = robot.position.turn_right
  end

  def report
    puts robot.report_current_position
  end
end
