# frozen_string_literal: true

require_relative 'lib/surface'
require_relative 'lib/robot'
require_relative 'lib/position'

require_relative 'services/parser'
require_relative 'services/command'

robot = Robot.new
surface = Surface.new(length: 5, width: 5)
parser = Robot::Parser.new(robot: robot, surface: surface)

puts "Robot is ready, please provide command ..."

# loop until a quit command is provided
while true
  puts "Please type QUIT in order to exit: "
  input = gets
  break if input =~ /quit/i

  command = parser.call(command: input)
  command.call unless command.nil?
end
