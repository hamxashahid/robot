# frozen_string_literal: true

require 'spec_helper'
require 'position'
require 'surface'

RSpec.describe Surface do

  subject { described_class.new(length: 5, width: 5) }

  describe '#position_valid??' do

    it 'returns true when both x and y coordinates are in range' do
      expect(subject.position_valid?(
          position: Position.new(x: 1, y: 1, direction: 'SOUTH')
      )).to be_truthy
    end

    it 'returns false when x coordinate is out of range are invalid' do
      expect(subject.position_valid?(
          position: Position.new(x: 6, y: 1, direction: 'SOUTH')
      )).to be_falsey
    end

    it 'returns false when y coordinate is out of range' do
      expect(subject.position_valid?(
          position: Position.new(x: 6, y: 1, direction: 'SOUTH')
      )).to be_falsey
    end

    it 'returns false when both coordinates are out of range' do
      expect(subject.position_valid?(
          position: Position.new(x: 6, y: -1, direction: 'SOUTH')
      )).to be_falsey
    end
  end
end
