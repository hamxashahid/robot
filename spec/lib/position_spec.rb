# frozen_string_literal: true

require 'spec_helper'
require 'position'

RSpec.describe Position do

  subject { described_class.new(x: 2, y: 2, direction: 'WEST')}

  describe '#move' do
    let(:move_method) { double('move_in_direction') }

    it 'calls the appropriate method to move in north direction' do
      expect(subject).to receive(:move_north)
      subject.move(direction: 'north')
    end

    it 'calls the appropriate method to move in east direction' do
      expect(subject).to receive(:move_east)
      subject.move(direction: 'east')
    end

    it 'calls the appropriate method to move in south direction' do
      expect(subject).to receive(:move_south)
      subject.move(direction: 'south')
    end

    it 'calls the appropriate method to move in west direction' do
      expect(subject).to receive(:move_west)
      subject.move(direction: 'west')
    end
  end

  describe '#turn_left' do

    context 'when current direction is north' do
      it 'starts facing west' do
        subject.direction = 'NORTH'
        expect(subject.turn_left).to eq('WEST')
      end
    end

    context 'when current direction is east' do
      it 'starts facing north' do
        subject.direction = 'EAST'
        expect(subject.turn_left).to eq('NORTH')
      end
    end

    context 'when current direction is south' do
      it 'starts facing east' do
        subject.direction = 'SOUTH'
        expect(subject.turn_left).to eq('EAST')
      end
    end

    context 'when current direction is west' do
      it 'starts facing south' do
        subject.direction = 'WEST'
        expect(subject.turn_left).to eq('SOUTH')
      end
    end
  end

  describe '#turn_right' do

    context 'when current direction is north' do
      it 'starts facing east' do
        subject.direction = 'NORTH'
        expect(subject.turn_right).to eq('EAST')
      end
    end

    context 'when current direction is EAST' do
      it 'starts facing south' do
        subject.direction = 'EAST'
        expect(subject.turn_right).to eq('SOUTH')
      end
    end

    context 'when current direction is south' do
      it 'starts facing west' do
        subject.direction = 'SOUTH'
        expect(subject.turn_right).to eq('WEST')
      end
    end

    context 'when current direction is west' do
      it 'starts facing north' do
        subject.direction = 'WEST'
        expect(subject.turn_right).to eq('NORTH')
      end
    end
  end

  describe '#move_north' do

    it 'moves one step to the north' do
      subject.direction = 'NORTH'
      new_position = subject.send(:move_north)
      expect(new_position.y).to eq (subject.y + 1)
      expect(new_position.x).to eq (subject.x)
      expect(new_position.direction).to eq (subject.direction)
    end

  end

  describe '#move_east' do

    it 'moves one step to the east' do
      subject.direction = 'EAST'
      new_position = subject.send(:move_east)
      expect(new_position.x).to eq (subject.x + 1)
      expect(new_position.y).to eq (subject.y)
      expect(new_position.direction).to eq (subject.direction)
    end

  end

  describe '#move_south' do

    it 'moves one step to the south' do
      subject.direction = 'SOUTH'
      new_position = subject.send(:move_south)
      expect(new_position.y).to eq (subject.y - 1)
      expect(new_position.x).to eq (subject.x)
      expect(new_position.direction).to eq (subject.direction)
    end

  end

  describe '#move_west' do

    it 'moves one step to the west' do
      subject.direction = 'WEST'
      new_position = subject.send(:move_west)
      expect(new_position.x).to eq (subject.x - 1)
      expect(new_position.y).to eq (subject.y)
      expect(new_position.direction).to eq (subject.direction)
    end

  end

end
