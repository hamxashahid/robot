# frozen_string_literal: true

require 'spec_helper'
require 'robot'
require 'position'
require 'surface'

RSpec.describe Robot do

  subject { described_class.new }
  let(:surface) { Surface.new(length: 5, width: 5) }

  describe '#report_current_position' do

    context 'when the robot is placed' do
      let(:position) { Position.new(x: 2, y: 3, direction: 'NORTH') }

      it 'reports position' do
        subject.position = position
        expect(subject.report_current_position).to eq('2,3,NORTH')
      end
    end

    context 'when the robot is not placed' do
      it 'reports error' do
        expect(subject.report_current_position).to eq('Robot is not placed yet.')
      end
    end
  end

  describe '#placed?' do

    context 'when the robot is not placed' do
      it 'returns false' do
        expect(subject.placed?).to be_falsey
      end
    end

    context 'when the robot is placed' do
      it 'returns true' do
        subject.position = Position.new(x: 1, y: 1, direction: 'WEST')
        expect(subject.placed?).to be_truthy
      end
    end
  end
end
