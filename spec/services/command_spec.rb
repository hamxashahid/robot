# frozen_string_literal: true

require'spec_helper'
require 'position'
require 'robot'
require 'surface'
require_relative '../../services/command'
require_relative '../../services/parser'


RSpec.describe Robot::Command do

  let(:robot) { Robot.new }
  let(:surface) { Surface.new(length: 5, width: 5) }

  subject { described_class }

  describe '#call' do

    context 'when command type is place' do
      let(:command_type) { 'place' }

      it 'calls parse method with given command type' do
        command = subject.new(robot: robot, surface: surface, command_type: command_type, params: {})
        expect(command).to receive(:place)
        command.call
      end
    end

    context 'when command type is move' do
      let(:command_type) { 'move' }

      it 'calls parse method with given command type' do
        command = subject.new(robot: robot, surface: surface, command_type: command_type, params: nil)
        expect(command).to receive(:move)
        command.call
      end
    end

    context 'when command type is left' do
      let(:command_type) { 'left' }

      it 'calls parse method with given command type' do
        command = subject.new(robot: robot, surface: surface, command_type: command_type, params: nil)
        expect(command).to receive(:left)
        command.call
      end
    end

    context 'when command type is right' do
      let(:command_type) { 'right' }

      it 'calls parse method with given command type' do
        command = subject.new(robot: robot, surface: surface, command_type: command_type, params: nil)
        expect(command).to receive(:right)
        command.call
      end
    end

    context 'when command type is report' do
      let(:command_type) { 'report' }

      it 'calls parse method with given command type' do
        command = subject.new(robot: robot, surface: surface, command_type: command_type, params: nil)
        expect(command).to receive(:report)
        command.call
      end
    end
  end

  describe '#place' do
    let(:command_type) { 'place' }

    context 'when the position is valid' do

      it 'places robot on the provided position' do
        command = subject.new(robot: robot, surface: surface, command_type: command_type, params: {x: 2, y: 3, direction: 'NORTH'})
        command.send(:place)
        expect(robot.position.x).to eq(2)
        expect(robot.position.y).to eq(3)
        expect(robot.position.direction).to eq('NORTH')
      end

    end

    context 'when the position is not valid' do

      it 'does nothing' do
        command = subject.new(robot: robot, surface: surface, command_type: command_type, params: {x: 2, y: 6, direction: 'NORTH'})
        command.send(:place)
        expect(robot.position).to be_nil
      end
    end
  end

  describe '#move' do
    let(:command_type) { 'move' }

    context 'when the robot is placed' do

      context 'when the next position is valid' do

        it 'places robot on the provided position' do
          robot.position = Position.new(x: 2, y: 3, direction: 'WEST')
          command = subject.new(robot: robot, surface: surface, command_type: command_type, params: nil)
          command.send(:move)
          expect(robot.position.x).to eq(1)
          expect(robot.position.y).to eq(3)
          expect(robot.position.direction).to eq('WEST')
        end
      end

      context 'when the next position not valid' do
        it 'does nothing and position remains unchanged' do
          robot.position = Position.new(x: 0, y: 0, direction: 'SOUTH')
          command = subject.new(robot: robot, surface: surface, command_type: command_type, params: nil)
          command.send(:move)
          expect(robot.position.x).to eq(0)
          expect(robot.position.y).to eq(0)
          expect(robot.position.direction).to eq('SOUTH')
        end
      end
    end

    context 'when the robot is not placed' do

      it 'does nothing' do
        robot.position = nil
        command = subject.new(robot: robot, surface: surface, command_type: command_type, params: nil)
        command.send(:move)
        expect(robot.position).to be_nil
      end
    end
  end

  describe '#left' do
    let(:command_type) { 'left' }

    context 'when the robot is placed' do

      it 'turns the robot towards left' do
        robot.position = Position.new(x: 2, y: 3, direction: 'WEST')
        command = subject.new(robot: robot, surface: surface, command_type: command_type, params: nil)
        command.send(:left)
        expect(robot.position.x).to eq(2)
        expect(robot.position.y).to eq(3)
        expect(robot.position.direction).to eq('SOUTH')
      end

    end

    context 'when the robot is not placed' do

      it 'does nothing' do
        robot.position = nil
        command = subject.new(robot: robot, surface: surface, command_type: command_type, params: nil)
        command.send(:left)
        expect(robot.position).to be_nil
      end
    end
  end

  describe '#right' do
    let(:command_type) { 'right' }

    context 'when the robot is placed' do

      it 'turns the robot towards left' do
        robot.position = Position.new(x: 2, y: 3, direction: 'WEST')
        command = subject.new(robot: robot, surface: surface, command_type: command_type, params: nil)
        command.send(:right)
        expect(robot.position.x).to eq(2)
        expect(robot.position.y).to eq(3)
        expect(robot.position.direction).to eq('NORTH')
      end

    end

    context 'when the robot is not placed' do

      it 'does nothing' do
        robot.position = nil
        command = subject.new(robot: robot, surface: surface, command_type: command_type, params: nil)
        command.send(:right)
        expect(robot.position).to be_nil
      end
    end
  end

  describe '#report' do
    let(:command_type) { 'report' }

    it 'calls report_current_position on robot' do
      robot.position = Position.new(x: 1, y: 1, direction: 'EAST')
      command = subject.new(robot: robot, surface: surface, command_type: command_type, params: nil)
      expect(robot).to  receive(:report_current_position)
      command.send(:report)
    end
  end
end
