# frozen_string_literal: true

require'spec_helper'
require 'position'
require 'robot'
require 'surface'
require_relative '../../services/command'
require_relative '../../services/parser'


RSpec.describe Robot::Parser do

  let(:robot) { Robot.new }
  let(:surface) { Surface.new(length: 5, width: 5) }

  subject { described_class.new(robot: robot, surface: surface) }

  describe '#call' do
    let(:command) { 'MOVE' }

    it 'calls parse method with given command' do
      expect(subject).to receive(:parse).with(command)
      subject.call(command: command)
    end

  end

  describe '#parse' do

    context 'PLACE' do

      let(:input) {'PLACE 2,2,SOUTH'}

      it 'creates command with the correct attributes' do

        expect(Robot::Command).to receive(:new).with(robot: robot,
                                                     surface: surface,
                                                     command_type: 'place',
                                                     params: {x: '2', y: '2', direction: 'SOUTH'},
                                                    )
        subject.send(:parse, input)
      end

      it 'returns command object' do
        expect(subject.send(:parse, input)).to be_a Robot::Command
      end
    end

    context 'MOVE' do
      let(:input) {'MOVE'}

      it 'creates command with the correct attributes' do
        expect(Robot::Command).to receive(:new).with(robot: robot,
                                                     surface: surface,
                                                     command_type: 'move',
                                                     params: nil,
                                                     )

        subject.send(:parse, input)
      end

      it 'returns command object' do
        expect(subject.send(:parse, input)).to be_a Robot::Command
      end
    end

    context 'LEFT' do
      let(:input) {'LEFT'}

      it 'creates command with the correct attributes' do
        expect(Robot::Command).to receive(:new).with(robot: robot,
                                                     surface: surface,
                                                     command_type: 'left',
                                                     params: nil,
                                                     )

        subject.send(:parse, input)
      end

      it 'returns command object' do
        expect(subject.send(:parse, input)).to be_a Robot::Command
      end
    end

    context 'RIGHT' do
      let(:input) {'RIGHT'}

      it 'creates command with the correct attributes' do
        expect(Robot::Command).to receive(:new).with(robot: robot,
                                                     surface: surface,
                                                     command_type: 'right',
                                                     params: nil,
                                                     )

        subject.send(:parse, input)
      end

      it 'returns command object' do
        expect(subject.send(:parse, input)).to be_a Robot::Command
      end
    end

    context 'REPORT' do
      let(:input) {'REPORT'}

      it 'creates command with the correct attributes' do
        expect(Robot::Command).to receive(:new).with(robot: robot,
                                                     surface: surface,
                                                     command_type: 'report',
                                                     params: nil,
                                                     )

        subject.send(:parse, input)
      end

      it 'returns command object' do
        expect(subject.send(:parse, input)).to be_a Robot::Command
      end
    end

    context 'INVALID command' do
      let(:input) {'xyz'}

      it 'returns nil' do
        expect(subject.send(:parse, input)).to be nil
      end
    end
  end
end
