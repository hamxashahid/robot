# frozen_string_literal: true

class Surface

  def initialize(length:, width:)
    self.length = length
    self.width = width
  end

  # valid positions are in range of 0 to (length - 1) and 0 to (width -1)
  def position_valid?(position:)
    (0..(length - 1)).include?(position.x) && (0..(width - 1)).include?(position.y)
  end

  private
  attr_accessor :length, :width
end
