# frozen_string_literal: true

class Position
  attr_accessor :x, :y, :direction

  DIRECTIONS = %w(NORTH EAST SOUTH WEST)

  def initialize(x:, y:, direction:)
    self.x = x
    self.y = y
    self.direction = direction
  end

  # move one step in current direction
  def move(direction:)
    self.send("move_#{direction}") if self.respond_to?("move_#{direction}", :include_private)
  end

  # turns 90 degrees counter clockwise
  def turn_left
    DIRECTIONS[(DIRECTIONS.index(direction) - 1) % 4]
  end

  # turns 90 degrees clockwise
  def turn_right
    DIRECTIONS[(DIRECTIONS.index(direction) + 1) % 4]
  end

  private

  def move_north
    Position.new(x: x, y: (y + 1), direction: direction)
  end

  def move_east
    Position.new(x: (x + 1), y: y, direction: direction)
  end

  def move_south
    Position.new(x: x, y: (y - 1), direction: direction)
  end

  def move_west
    Position.new(x: (x - 1), y: y, direction: direction)
  end
end
