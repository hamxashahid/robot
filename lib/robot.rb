# frozen_string_literal: true

class Robot
  attr_accessor :position

  def initialize
    self.position = nil
  end

  def report_current_position
    if placed?
      "#{position.x},#{position.y},#{position.direction}"
    else
      'Robot is not placed yet.'
    end
  end

  def placed?
    !position.nil?
  end
end
